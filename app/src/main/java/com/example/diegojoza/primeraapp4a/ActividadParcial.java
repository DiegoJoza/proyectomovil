package com.example.diegojoza.primeraapp4a;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.zip.Inflater;

public class ActividadParcial extends AppCompatActivity {
    Button botonlogin, botonbuscar, botonregistrar, botonpasar, botonfragmento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_parcial);

        botonlogin = findViewById(R.id.btnlogin);
        botonbuscar =  findViewById(R.id.btnbuscar);
        botonregistrar =  findViewById(R.id.btnregistrar);
        botonpasar = findViewById(R.id.parametro);
        botonfragmento = findViewById(R.id.fragmento);

        botonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadLogin.class);
                startActivity(intent);
            }
        });


        botonregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadRegistrar.class);
                startActivity(intent);
            }
        });

        botonbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadBuscar.class);
                startActivity(intent);
            }
        });

        botonpasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadPasarParametro.class);
                startActivity(intent);
            }
        });
        botonfragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, Fragmento.class);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionlogin:
                //intent = new Intent(ActividadParcial.this, ActividadLogin.class);
                //startActivity(intent);
                Dialog dialogoLogin = new Dialog(ActividadParcial.this);
                dialogoLogin.setContentView(R.layout.dlg_log);

                Button botonAutenticar = dialogoLogin.findViewById(R.id.btnautenticar);
                final EditText cajaUsuario = dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = dialogoLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadParcial.this, cajaUsuario.getText().toString() + " " + cajaClave.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });


                dialogoLogin.show();
                break;
            case R.id.opcionregistrar:
                intent = new Intent(ActividadParcial.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
        }
        return true;
    }

}
