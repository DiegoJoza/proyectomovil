package com.example.diegojoza.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividadPasarParametro extends AppCompatActivity {

    EditText cajaDATOS;
    Button botonenviarP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pasar_parametro);
        cajaDATOS = findViewById(R.id.cajadato);
        botonenviarP = findViewById(R.id.enviarparametro);

        botonenviarP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPasarParametro.this, VisualizarParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",cajaDATOS.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}
