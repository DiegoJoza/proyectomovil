package com.example.diegojoza.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class VisualizarParametro extends AppCompatActivity {
    TextView texto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_parametro);
        texto = findViewById(R.id.textREC);
        Bundle bundle = this.getIntent().getExtras();
        texto.setText(bundle.getString("dato"));

    }
}
